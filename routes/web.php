<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswa', 'SiswaController@index');
Route::post('/siswa/create', 'SiswaController@create');
Route::get('/siswa/{id}/edit', 'SiswaController@edit');
Route::post('/siswa/{id}/update', 'SiswaController@update');
Route::get('/siswa/{id}/delete', 'SiswaController@delete');

Route::get('/sekolah', 'SekolahController@new_data');
Route::post('/sekolah/create', 'SekolahController@create');
Route::get('/sekolah/{id}/edit', 'SekolahController@edit');
Route::post('/sekolah/{id}/update', 'SekolahController@update');
Route::get('/sekolah/{id}/delete', 'SekolahController@delete');
