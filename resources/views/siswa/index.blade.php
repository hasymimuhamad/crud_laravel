@extends('layout.master')

@section('content')
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                      {{session('success')}}
            </div>
            @endif
             
            <div class="row">
                <div class="col-6">
                    <h1>Data Siswa</h1>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
                                Tambah Data Siswa
                    </button>        
                </div>
                

                <table class="table table-hover">
                    <tr>
                        <th>NAMA DEPAN</th>
                        <th>NAMA BELAKANG</th>
                        <th>JENIS KELAMIN</th>
                        <th>AGAMA</th>
                        <th>ALAMAT</th>
                        <th>PROSES</th>
                    </tr>        
                                
                    @foreach ($data_siswa as $siswa)
                    <tr>
                        <th>{{$siswa->nama_depan}}</th>
                        <th>{{$siswa->nama_belakang}}</th>
                        <th>{{$siswa->jenis_kelamin}}</th>
                        <th>{{$siswa->agama}}</th>
                        <th>{{$siswa->alamat}}</th>
                        <th>
                            <a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <a href="/siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm">Delete</a>
                        </th>
                    </tr>
                    @endforeach
                            
                </table>
            </div>
        </div>
      
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Input Siswa</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                            <form action="/siswa/create" method="POST">
                                {{csrf_field()}}
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nama Depan</label>
                                      <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan">
                                      <small id="emailHelp" class="form-text text-muted">NB : a - z</small>
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nama Belakang</label>
                                      <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Belakang">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" class="custom-select">
                                            <option value = "L" >Laki-laki</option>
                                            <option value="P" >Perempuan</option>
                                        </select>
                                    </div>
                                   
                                    <div class="form-group">
                                            <label for="exampleInputPassword1">Agama</label>
                                            <select name="agama" class="custom-select">
                                                <option value="Hindu" >Hindu</option>
                                                <option value="Islam" >Islam</option>
                                                <option value="Kristen" >Kristen</option>
                                                <option value="Lainnya" >Lainnya</option>
                                            </select>
                                    </div>

                                    <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Alamat</label>
                                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                          </div>
                                          <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                          </div>
                                    </form>
                    
                   
                  
                </div>
        </div>                  
@endsection


