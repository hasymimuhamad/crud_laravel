@extends('layout.master')

@section('content')
            <h1>Edit Data Siswa</h1>
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                      {{session('success')}}
            </div>
            @endif
             
            <div class="row">
                <div class="col-lg-12">
                   
                <form action="/siswa/{{$siswa->id}}/update" method="POST">
                        {{csrf_field()}}
                
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama Depan</label>
                            <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan" value="{{$siswa -> nama_depan}}">
                              <small id="emailHelp" class="form-text text-muted">NB : a - z</small>
                            </div>

                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama Belakang</label>
                              <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Belakang" value="{{$siswa -> nama_belakang}}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="custom-select">
                                    <option value = "L" @if ($siswa == 'L') selected @endif >Laki-laki</option>
                                    <option value="P" @if ($siswa == 'P') selected @endif >Perempuan</option>
                                </select>
                            </div>
                           
                            <div class="form-group">
                                <label for="exampleInputPassword1">Agama</label>
                                <select name="agama" class="custom-select" value="{{$siswa -> agama}}">
                                    <option value="Hindu" >Hindu</option>
                                    <option value="Islam" >Islam</option>
                                    <option value="Kristen" >Kristen</option>
                                    <option value="Lainnya" >Lainnya</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Alamat</label>
                                <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" > {{$siswa -> alamat}} </textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-warning">Update</button>
                            </div>
                    </form>
                
                </div>
            </div>
        </div>
      
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Siswa</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                </div>    
            </div>
        </div>                  
@endsection