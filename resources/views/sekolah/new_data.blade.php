@extends('layout.master')

@section('content')
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                      {{session('success')}}
            </div>
            @endif
             
            <div class="row">
                <div class="col-6">
                    <h1>Data Sekolah</h1>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
                                Tambah Data Sekolah
                    </button>        
                </div>
                

                <table class="table table-hover">
                    <tr>
                        <th>NAMA SEKOLAH</th>
                        <th>JURUSAN</th>
                
                        <th>ALAMAT</th>
                        <th>PROSES</th>
                    </tr>        
                                
                    @foreach ($data_sekolah as $sekolah)
                    <tr>
                        <th>{{$sekolah->nama_sekolah}}</th>
                        <th>{{$sekolah->jurusan}}</th>
                        <th>{{$sekolah->alamat}}</th>
                        
                        <th>
                            <a href="/sekolah/{{$sekolah->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <a href="/sekolah/{{$sekolah->id}}/delete" class="btn btn-danger btn-sm">Delete</a>
                        </th>
                    </tr>
                    @endforeach
                            
                </table>
            </div>
        </div>
      
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Input Data Sekolah</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                            <form action="/sekolah/create" method="POST">
                                {{csrf_field()}}
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nama Sekolah</label>
                                      <input name="nama_sekolah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan">
                                      <small id="emailHelp" class="form-text text-muted">NB : a - z</small>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jurusan</label>
                                        <select name="jurusan" class="custom-select">
                                            <option value = "IPA" >IPA</option>
                                            <option value="IPS" >IPS</option>
                                            <option value="DLL" >Lainnya</option>
                                        </select>
                                    </div>
                                   
                                 

                                    <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Alamat</label>
                                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                          </div>
                                          <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                          </div>
                                    </form>
                    
                   
                  
                </div>
        </div>                  
@endsection


