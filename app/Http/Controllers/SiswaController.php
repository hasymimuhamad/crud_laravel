<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Siswa;

class SiswaController extends Controller
{
    public function index()
    {
        $data_siswa = Siswa::get();
        // dd($data_siswa);
        return view('siswa.index',  compact('data_siswa'));
        
    }

    public function create(Request $request)
    {
        Siswa::create($request->all());
        return redirect('/siswa') -> with('success', 'Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $siswa = Siswa::find($id);
        return view('siswa.edit', compact('siswa'));
    }
    
    public function update(Request $request, $id)
    {
        $siswa = Siswa::find($id);
        $siswa-> update($request->all()); 

        return redirect('/siswa') -> with ('success', 'Data Berhasil di update');
    }

    public function delete($id)
    {
        $siswa = Siswa::find($id);
        $siswa->delete();
        return redirect('/siswa')->with('success', 'Data Berhasil di hapus');
    }
    
}
