<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Sekolah;

class SekolahController extends Controller
{
    public function new_data()
    {
        $data_sekolah = Sekolah::get();
        return view('sekolah.new_data', compact('data_sekolah'));
    }


    public function create(Request $request)
    {
        Sekolah::create($request->all());
        return redirect('/sekolah') -> with('success', 'Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $data_sekolah = Sekolah::find($id);
        return view('sekolah.edit_sekolah', compact('data_sekolah'));
    }
    
    public function update(Request $request, $id)
    {
        $data_sekolah = Sekolah::find($id);
        $data_sekolah-> update($request->all()); 

        return redirect('/sekolah') -> with ('success', 'Data Berhasil di update');
    }

    public function delete($id)
    {
        $data_sekolah = Sekolah::find($id);
        $data_sekolah->delete();
        return redirect('/sekolah')->with('success', 'Data Berhasil di hapus');
    }
}
