<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table = 'data_sekolah';
    protected $fillable = ['nama_sekolah', 'alamat', 'jurusan'];
}